class Board {
  constructor (x, y, xCells, yCells, context) {
    this.cellSize = 25
    this.position = {
      x: x,
      y: y
    }
    this.cellCount = {
      x: xCells,
      y: yCells
    }
    this.context = context

    this.boardGraphics = this.context.add.graphics({
      x: this.position.x,
      y: this.position.y
    })

    this.levelData = [...Array(this.cellCount.y)].map(x=>Array(this.cellCount.x).fill(null))

    this.hints = {
      rows: Array(this.cellCount.y).fill([]),
      cols: Array(this.cellCount.x).fill([])
    }

    this.text = []
  }

  clearText () {
    for(let text of this.text) {
      text.destroy()
    }
  }

  setCell (x, y, value) {
    this.levelData[y][x] = value
  }

  redraw () {
    this.boardGraphics.clear()
    this.boardGraphics.lineStyle(2, 0x000000)
    this.boardGraphics.fillStyle(0xFFFFFF, 1.0)
    for(let j=0; j<this.cellCount.y; j++){
      for(let i=0; i<this.cellCount.x; i++){
        let cell = this.levelData[j][i]
        if(cell != null) {
          this.boardGraphics.fillStyle(0x000000, 1.0)
        } else {
          this.boardGraphics.fillStyle(0xFFFFFF, 1.0)
        }
        this.boardGraphics.fillRect(i * this.cellSize, j * this.cellSize, this.cellSize, this.cellSize)
        this.boardGraphics.strokeRect(i * this.cellSize, j * this.cellSize, this.cellSize, this.cellSize)
      }
    }
  }

  checkClick (x, y) {
    if( x >= this.position.x &&
        y >= this.position.y &&
        x <= this.position.x+(this.cellCount.x*this.cellSize) &&
        y <= this.position.y+(this.cellCount.y*this.cellSize)) {
      let i = Math.floor((x - this.position.x) / this.cellSize)
      let j = Math.floor((y - this.position.y) / this.cellSize)
      if (this.levelData[j][i]) {
        this.levelData[j][i] = null
      } else {
        this.levelData[j][i] = {
          color: 1
        }
      }
      this.updateHints(i, j)
      //displayHints()
    }
  }

  updateHints (col, row) {
    this.hints.rows[row] = []
    this.hints.cols[col] = []
    let rowData = this.levelData[row]
    let colData = []
    for (let i = 0; i<this.levelData[row].length; i++) {
      colData.push(this.levelData[i][col])
    }

    let count = 0
    for (let i=0; i<rowData.length; i++) {
      if (rowData[i]) {
        count++
      } else {
        if (count != 0) {
          this.hints.rows[row].push(count)
        }
        count = 0
      }
    }
    if (count != 0) {
      this.hints.rows[row].push(count)
    }

    count = 0
    for (let i=0; i<colData.length; i++) {
      if (colData[i]) {
        count++
      } else {
        if (count != 0) {
          this.hints.cols[col].push(count)
        }
        count = 0
      }
    }
    if (count != 0) {
      this.hints.cols[col].push(count)
    }

    this.displayHints()
  }

  displayHints () {
    this.clearText()
    for (let i = 0; i < this.hints.rows.length; i++) {
      let hint = this.hints.rows[i].slice().reverse()
      if (hint) {
        let x = this.position.x - this.cellSize
        let y = this.position.y + this.cellSize * i
        if (hint.length == 0) {
          let t = this.context.add.text(
            x,
            y,
            '0',
            {
              fontSize: this.cellSize + 'px',
              fill: '#000'
            }
          )
          this.text.push(t)
        } else {
          for(let [key, value] of hint.entries()) {
            let t = this.context.add.text(
              x - (this.cellSize * key),
              y,
              value,
              {
                fontSize: this.cellSize + 'px',
                fill: '#000'
              }
            )
            this.text.push(t)
          }
        }
      }
    }
    
    for (let i = 0; i < this.hints.cols.length; i++) {
      let hint = this.hints.cols[i].slice().reverse()
      if (hint) {
        let x = this.position.x + this.cellSize * i
        let y = this.position.y - this.cellSize
        if (hint.length == 0) {
          let t = this.context.add.text(
            x,
            y,
            '0',
            {
              fontSize: this.cellSize + 'px',
              fill: '#000'
            }
          )
          this.text.push(t)
        } else {
          for(let [key, value] of hint.entries()) {
            let t = this.context.add.text(
              x,
              y - (this.cellSize * key),
              value,
              {
                fontSize: this.cellSize + 'px',
                fill: '#000'
              }
            )
            this.text.push(t)
          }
        }
      }
    }
  }
}

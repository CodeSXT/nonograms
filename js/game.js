// Create a new Scene
let gameScene = new Phaser.Scene('Game')
let cursors;
let player;

gameScene.preload = function () {
  this.load.image('bg', 'assets/bg.png')
  this.load.image('chara', 'assets/sprites/character.png')
}

gameScene.create = function () {
  let bg = this.add.sprite(0, 0, 'bg')
  player = this.add.sprite(0, 0, 'chara')
  // bg.setOrigin(0, 0)
  bg.setPosition(this.sys.game.config.width/2, this.sys.game.config.height/2)
  player.setPosition(this.sys.game.config.width/2, this.sys.game.config.height/2)

  cursors = this.input.keyboard.createCursorKeys()
}

gameScene.update = function () {
  if (cursors.left.isDown) {
    player.x--;
  } else if (cursors.right.isDown) {
    player.x++;
  } else if (cursors.up.isDown) {
    player.y--;
  } else if (cursors.down.isDown) {
    player.y++;
  }
}

// Create a configuration for the game
let config = {
  type: Phaser.AUTO,
  width: 640,
  height: 360,
  scene: gameScene
}

// Create a new game with the configuration
let game = new Phaser.Game(config)

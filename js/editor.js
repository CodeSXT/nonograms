// Create a new Scene
let gameScene = new Phaser.Scene('Game Editor')
// let cursors
// let palette = []

gameScene.preload = function () {
  this.load.image('bg', 'assets/bg.png')
}

gameScene.create = function () {
  let bg = this.add.sprite(0, 0, 'bg')
  // bg.setOrigin(0, 0)
  bg.setPosition(this.sys.game.config.width/2, this.sys.game.config.height/2)
  bg.setScale(4.0)

  this.gameBoard = new Board(370, 100, 10, 10, this)
  this.gameBoard.setCell(4, 8, {color:1})
  this.gameBoard.checkClick(4, 8)
  this.gameBoard.displayHints()
  // console.log(this.gameBoard.levelData)

  // cursors = this.input.keyboard.createCursorKeys()
  this.input.on('pointerdown', pointerClickCallback, this)
}

gameScene.update = function () {
  this.gameBoard.redraw()
}

pointerClickCallback = function (pointer) {
  this.gameBoard.checkClick(pointer.x, pointer.y)
}

// Create a configuration for the game
let config = {
  type: Phaser.AUTO,
  width: 640,
  height: 360,
  pixelArt: true,
  scene: gameScene
}

// Create a new game with the configuration
let game = new Phaser.Game(config)
